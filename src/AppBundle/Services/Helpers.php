<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class Helpers
{
    public function json($data)
    {

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $normalizers[0]->setIgnoredAttributes(array('password'));
        $serializer = new Serializer($normalizers, $encoders);

        $response = new Response($serializer->serialize($data, 'json'));
        $response->headers->set("Content-Type", "application/json");

        return $response;
    }
}