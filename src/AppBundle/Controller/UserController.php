<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class UserController extends FOSRestController
{

    /**
     * Create User
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction(Request $request)
    {
        // Get User Manager
        $userManager = $this->get('fos_user.user_manager');

        // Create empty user
        $user = $userManager->createUser();

        // Activate user
        $user->setEnabled(true);

        // Retrieve values from POST
        $dni = $request->get('dni');
        $name = $request->get('name');
        $surname = $request->get('surname');
        $email = $request->get('email');

        // Required values
        if ($dni != null && $name != null && $surname != null) {
            $user->setDni($dni)
                ->setName(strtoupper($name))
                ->setSurname(strtoupper($surname));

            // Duplicate username verification
            $username = strtolower(substr($user->getName(), 0, 1) . $user->getSurname());
            $i = 0;
            $usernameExists = count($userManager->findUserByUsername($username)) == 1;
            while($usernameExists){
                $usernameExists = count($userManager->findUserByUsername($username . $i)) == 1;
                $i++;
            }
            if ($i > 0) $user->setUsername($username . ($i-1));
            else $user->setUsername($username);

            // Set optional Email field
            if ($email != null) $user->setEmail($email);

            // Validate User data
            $validator = $this->get('validator');
            $errors = $validator->validate($user);

            if (count($errors) == 0) {
                $userManager->updateUser($user);
                // Return User data
                $view = $this->view(['user' => $user], 200);
            } else {
                // Return validation errors
                $view = $this->view(['error' => $errors], 400);
            }
        } else {
            $view = $this->view(["error" => "Fields cannot be empty"], 400);
        }
        return $this->handleView($view);
    }

    /** The approach with this delete action was to not lose any data,
     *  but instead set to false the Enabled field of User
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id)
    {
        // Get User Manager
        $userManager = $this->get('fos_user.user_manager');

        // Get User by ID
        $user = $userManager->findUserBy(array('id'=>$id));

        if (count($user) == 1) {
            if (!$user->isEnabled()) {
                $view = $this->view(["message" => "User does not exists"], 400);
            } else {
                // Set user to disabled and update
                $user->setEnabled(false);
                $userManager->updateUser($user);
                $view = $this->view(["message" => "User has been deleted"], 200);
            }
        } else {
            $view = $this->view(["message" => "User does not exists"], 400);
        }
        return $this->handleView($view);
    }

    /**
     * Update User data
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function patchAction(Request $request, $id)
    {
        // Get User Manager
        $userManager = $this->get('fos_user.user_manager');

        // Get User by ID
        $user = $userManager->findUserBy(array('id'=>$id));

        if (count($user) == 1) {

            // Retrieve values from PATCH
            $dni = $request->get('dni');
            $name = $request->get('name');
            $surname = $request->get('surname');
            $email = $request->get('email', $user->getEmail());

            // If any value is not null, then update
            if ($dni != null || $name != null || $surname != null || $email != null) {
                if ($dni != null) $user->setDni($dni);
                if ($name != null) $user->setName(strtoupper($name));
                if ($surname != null) $user->setSurname(strtoupper($surname));
                if ($email == "") $user->setEmail(null); elseif ($email != null) $user->setEmail($email);

                // Duplicate username verification
                $username = strtolower(substr($user->getName(), 0, 1) . $user->getSurname());
                $i = 0;

                $userSearch = $userManager->findUserByUsername($username);
                $usernameExists = count($userSearch) == 1 && $user->getId() != $userSearch->getId();
                while($usernameExists){
                    $userSearch = $userManager->findUserByUsername($username . $i);
                    $usernameExists = count($userSearch) == 1 && $user->getDni() != $userSearch->getDni();
                    $i++;
                }
                if ($i > 0) $user->setUsername($username . ($i-1));
                else $user->setUsername($username);

                // Validate User data
                $validator = $this->get('validator');
                $errors = $validator->validate($user);

                if (count($errors) == 0) {
                    // Set user to enabled and update
                    $user->setEnabled(true);
                    $userManager->updateUser($user);
                    $view = $this->view(['user' => $user], 200);
                } else {
                    $view = $this->view(['error' => $errors], 400);
                }
            } else {
                $view = $this->view(["message" => "Nothing to patch"], 400);
            }
        } else {
            $view = $this->view(["message" => "User does not exists"], 400);
        }
        return $this->handleView($view);
    }

    /**
     * List all Users
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        // Get User Manager
        $userManager = $this->get('fos_user.user_manager');

        // Get every User in database
        $users = $userManager->findUsers();

        if (count($users) > 0) {
            $view = $this->view(['users' => $users], 200);
        } else {
            $view = $this->view(['message' => "No users in database"],  400);
        }
        return $this->handleView($view);
    }
}
