ClientBackend
=============
_________________
### Consigna
##### Desarrollar una API que permita:
1. Crear un Cliente (datos a recibir: Nombre, Apellido, DNI, Email; datos requeridos: Nombre, Apellido, DNI).
2. Eliminar un Cliente.
3. Actualizar los datos de un cliente.
5. Listar los clientes existentes.
6. Antes de guardar un nuevo cliente los campos de nombre y apellido deben guardarse en mayúscula.

##### Se valorara:
1. Utilizar Synfony 3.2
2. Segurización de la API.
3. Prolijidad, estructura y comentarios en el código.
4. Validación de tipos de datos.
5. Utilización de composer.
6. Utilzación de bundles tales como FOSUserBundle y FOSOAuthServerBundle.

### Instrucciones de uso
1. Clonar el repositorio
    - `git clone https://bitbucket.org/AntuV/clientsbackend.git`
2. Entrar al directorio
    - `cd clientsbackend`
3. Instalar dependencias
    - `composer install`
4. Configurar credenciales de MySQL (default: root, sin contraseña) y ejecutar los siguientes comandos:
    - `php bin/console doctrine:database:create`
    - `php bin/console doctrine:schema:create`
    - `php bin/console doctrine:schema:update --force`
8. Crear Cliente OAuth con el siguiente comando, cambiando el host y puerto si es necesario
    - `php bin/console app:oauth-server:client:create --redirect-uri="http://127.0.0.1:8000/" --grant-type="client_credentials"`

### API


| Name                       | Method   | Scheme | Host | Path        |
| -------------------------- | -------- | --- | --- | --------------- |
| fos_oauth_server_token     | GET / POST | ANY | ANY | /oauth/v2/token |
| post                       | POST     | ANY | ANY | /user/          |
| delete                     | DELETE   | ANY | ANY | /user/{id}      |
| patch                      | PATCH    | ANY | ANY | /user/{id}      |
| list                       | GET      | ANY | ANY | /user/list      |
